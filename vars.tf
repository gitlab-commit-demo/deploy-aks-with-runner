# K8s version
variable "kubernetes_version" {
  default     = "1.13.8"
}

# Cluster name
variable cluster_name {
    default = "demo-aks-1"
}

# Cluster DNS prefix
variable "dns_prefix" {
    default = "demo-aks-1"
}

# Resource Group
variable resource_group_name {
    default = "demo-aks-rg"
}

# Node location
variable location {
    default = "East US"
}

# Node count
variable "agent_count" {
    default = 1
}

# Node size
variable "agent_vm_size" {
    default = "Standard_B2s"
}

# Node local disk size
variable "agent_os_disk_size" {
    default = "30"
}

# Node local user
variable "user_name" {
    default = "tf"
}

# variable exposed via cli
variable "client_id" {}
variable "client_secret" {}
variable "rbac_server_app_id" {}
variable "rbac_server_app_secret" {}
variable "rbac_client_app_id" {}
variable "tenant_id" {}
variable "public_key" {}
