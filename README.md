# Gitlab Commit Demo

## Runner environment

This repo is part of my Demo at Gitlab Commit. The repo can be used to build a demo environment based on Azure Kubernetes Service and Gitlab Runner Kubernetes executor.

## TL;DR

1. Clone this repo
2. Export following project variables
    - `tenentId` containing your Azure Tenant ID
    - `subscriptionId` containing your Azure subscription
    - `azUser` containing your Azure User
    - `azPsw` containing your Azure User password
    - `runnerToken` containing the Runner token of your GitLab Project/Group
3. Execute `prep/prep.sh` to setup requirements for AKS & Terraform
4. Execute the pipeline
