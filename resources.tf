# creates a resource group
resource "azurerm_resource_group" "aks" {
    name     = "${var.resource_group_name}"
    location = "${var.location}"
}

#creates the aks cluster
resource "azurerm_kubernetes_cluster" "aks" {
    name                = "${var.cluster_name}"
    location            = "${azurerm_resource_group.aks.location}"
    resource_group_name = "${azurerm_resource_group.aks.name}"
    dns_prefix          = "${var.dns_prefix}"
    kubernetes_version  = "${var.kubernetes_version}"

    linux_profile {
        admin_username = "${var.user_name}"

        ssh_key {
            key_data = "${var.public_key}"
        }
    }

    #defines the aks nodes
    agent_pool_profile {
        name            = "agentpool"
        count           = "${var.agent_count}"
        vm_size         = "${var.agent_vm_size}"
        os_type         = "Linux"
        os_disk_size_gb = "${var.agent_os_disk_size}"
    }

    # mapped service principal
    service_principal {
        client_id     = "${var.client_id}"
        client_secret = "${var.client_secret}"
    }

    # aks addons
    addon_profile {
        http_application_routing {
            enabled = false
        }
    }

    # rbac configuration
    role_based_access_control {
        azure_active_directory {
            server_app_id     = "${var.rbac_server_app_id}"
            server_app_secret = "${var.rbac_server_app_secret}"
            client_app_id     = "${var.rbac_client_app_id}"
            tenant_id         = "${var.tenant_id}"
        }
        enabled = true
    }
}
